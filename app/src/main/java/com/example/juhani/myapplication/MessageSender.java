package com.example.juhani.myapplication;

import android.app.Activity;
import android.os.Message;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Juhani on 21.9.15.
 */
public class MessageSender extends Activity {
    private Socket mSocket;
    private Message mMessage;

    //
    public MessageSender(Socket socket, Message message){
        if(socket== null) return;
        if(message==null) return;


        this.mSocket=mSocket;
        this.mMessage=mMessage;
        new Thread(new SendThread()).start();
    }

    class SendThread implements Runnable{
        @Override
        public void run(){
            try{
                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(mSocket.getOutputStream())),
                        true);
            }catch (Exception exception){
                exception.printStackTrace();
            }
        }
    }

}
