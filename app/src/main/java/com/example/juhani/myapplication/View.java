package com.example.juhani.myapplication;

import android.app.Activity;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.net.Socket;

/**
 * Created by Juhani on 21.9.15.
 */
public class View extends Activity {
    //HAETAAN WIDGETIT XML:STÄ
    private Socket connection;
    private Message msg;
    private Button button;
    private EditText textEdit;
    private ScrollView scrolli;
    private TextView textfield;

    public EditText getTextEdit() {
        return textEdit;
    }

    public Button getButton() {
        return button;
    }

    public ScrollView getScrolli() {
        return scrolli;
    }

    public Socket getConnection() {
        return connection;
    }
}
